using System;
using Core.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nest;

namespace Data
{
    public static class ElasticsearchExtensions
    {
        public static void AddElasticsearch(this IServiceCollection services, IConfiguration cfg)
        {
            var url = Environment.GetEnvironmentVariable("ELASTICSEARCH_URL") ??
                      cfg["elasticsearch:url"];

            var defaultIndex = "posts";

            var settings = new ConnectionSettings(new Uri(url))
                .DefaultIndex(defaultIndex);

            AddDefaultMappings(settings);

            var client = new ElasticClient(settings);

            services.AddSingleton(client);
            CreateIndex(client, defaultIndex);
        }

        private static void CreateIndex(ElasticClient client, string defaultIndex)
        {
            var createIndexResponse = client.Indices.Create(defaultIndex,
                i => i.Map<Post>(x => x.AutoMap()));
        }

        private static void AddDefaultMappings(ConnectionSettings settings)
        {
            settings
                .DefaultMappingFor<Post>(m => m);
        }
    }
}