using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Repositories;
using Elasticsearch.Net;
using Microsoft.EntityFrameworkCore;
using Nest;

namespace Data.Repositories
{
    public class PostRepository : IPostRepository
    {
        private readonly ElasticClient _elasticClient;

        public PostRepository(ElasticClient elasticClient)
        {
            _elasticClient = elasticClient;
        }

        public async Task<List<Post>> GetPostsByUserId(int userId, int page)
        {
            var result = await _elasticClient.SearchAsync<Post>(p => p
                .From(page * 10)
                .Size(10)
                .Sort(s => s.Descending(d => d.CreatedDate))
                .Source(s => s.Excludes(e => e.Field(f => f.Comments)))
                .Query(q => q
                    .Term(t => t
                        .Field(f => f.AuthorId)
                        .Value(userId))));

            return result.Documents.ToList();
        }

        public async Task<bool> LikeDislikePost(int requesterUserId, string postId)
        {
            Console.WriteLine($"PostRepository before update values: {requesterUserId}, {postId}");

            var updateResponse = await _elasticClient.UpdateByQueryAsync<Post>(ubq => ubq.Index("posts")
                .Conflicts(Conflicts.Proceed)
                .Query(q => q.Term(t => t.Field(f => f.Id).Value(postId)))
                .Script(s => s
                    .Source(
                        "if (ctx._source.containsKey('likedBy')) {" +
                            "if (ctx._source.likedBy.contains(params.requesterId)) {" +
                                "ctx._source.likedBy.remove(ctx._source.likedBy.indexOf(params.requesterId))" +
                            "}" +
                            "else {ctx._source.likedBy.add(params.requesterId)}" +
                        "} else {ctx._source.likedBy = [params.requesterId]}")
                    .Params(new Dictionary<string, object> {{"requesterId", requesterUserId}})
                    .Lang("painless")));

            Console.WriteLine($"PostRepository after update values: {requesterUserId}, {postId}");
            return updateResponse.IsValid;
        }

        public async Task<bool> AddComment(int commenterId, string postId, string comment)
        {
            var response = await _elasticClient.UpdateByQueryAsync<Post>(ubq => ubq.Index("posts")
                .Conflicts(Conflicts.Proceed)
                .Query(q => q.Term(t => t.Field(f => f.Id).Value(postId)))
                .Script(s => s
                    .Source(
                        "if (ctx._source.containsKey('comments')) {ctx._source.comments.add(params.comment)}" +
                        "else {ctx._source.comments = [params.comment]}")
                    .Params(new Dictionary<string, object> {{"comment", new Comment
                    {
                        Id = Guid.NewGuid(),
                        Content = comment,
                        AuthorId = commenterId,
                        CreatedDate = DateTimeOffset.Now
                    }}})
                    .Lang("painless")));
            return response.IsValid;
        }
        
        public async Task<List<Comment>> GetComments(string postId, int page)
        {
            var response =  await _elasticClient.SearchAsync<Post>(sa => sa.Query(q => q
                .Raw("{\"bool\":{\"filter\":[{\"term\":{\"_id\":\"" + postId + "\"}},{\"nested\":{\"path\":\"comments\",\"query\":{\"match_all\":{}},\"inner_hits\":{\"sort\":[{\"comments.createdDate\":\"asc\"}],\"from\":" + page * 10 +  ",\"size\":10}}}]}}")));

            List<Comment> comments;

            if (response.IsValid && response.Hits.Count > 0)
            {
                var hit = response.Hits.First();

                comments = hit.InnerHits["comments"].Hits.Hits
                    .Select(h => h.Source.As<Comment>())
                    .ToList();
            }
            else
            {
                comments = new List<Comment>();
            }
            
            return comments;
        }
        
        public async Task<bool> AddPost(int authorId, string description, string imageGuid)
        {
            var post = new Post
            {
                Id = Guid.NewGuid(),
                AuthorId = authorId,
                Description = description,
                ImageGuid = imageGuid,
                CreatedDate = DateTimeOffset.Now,
                LikedBy = new List<int>()
            };

            var response = await _elasticClient.IndexDocumentAsync(post);

            return response.IsValid;
        }

        public async Task<List<Post>> GetAllPostsOfUsersByIds(IEnumerable<int> ids, int page)
        {
            var response =  await _elasticClient.SearchAsync<Post>(p => p
                .Source(s => s.Excludes(e => e.Field(f => f.Comments)))
                .Sort(s => s.Descending(d => d.CreatedDate))
                .Skip(page * 10)
                .Take(10)
                .Query(q => q
                    .Terms(t => t
                        .Field(f => f.AuthorId)
                        .Terms(ids))));

            return response.Documents.ToList();
        }

        public async Task<List<Post>> GetFullTextSearchedPosts(string phrase)
        {
            var response = await _elasticClient.SearchAsync<Post>(p => p
                .Take(10)
                .Source(s => s.Excludes(e => e.Field(f => f.Comments)))
                .Query(q => q
                    .Fuzzy(f => f
                        .Field(fi => fi.Description)
                        .Fuzziness(Fuzziness.Auto)
                        .Value(phrase)
                        .Rewrite(MultiTermQueryRewrite.TopTerms(10))
                    )));

            return !response.IsValid ? new List<Post>() : response.Documents.ToList();
        }
    }
}