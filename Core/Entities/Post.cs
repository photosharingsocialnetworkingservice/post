using System;
using System.Collections.Generic;
using Nest;

namespace Core.Entities
{
    public class Post
    {
        public Guid Id { get; set; }
        public int AuthorId { get; set; }
        public string Description { get; set; }
        public string ImageGuid { get; set; }
        public List<int> LikedBy { get; set; } = new List<int>();
        public DateTimeOffset CreatedDate { get; set; }
        [Nested]
        public List<Comment> Comments { get; set; } = new List<Comment>();
    }
}