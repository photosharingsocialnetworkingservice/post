using System;

namespace Core.Entities
{
    public class Comment
    {
        public Guid Id { get; set; }
        public int AuthorId { get; set; }
        public string Content { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
    }
}