using AutoMapper;
using Core.Contracts;

namespace Core.AutoMapperProfiles
{
    public class PostProfile : Profile
    {
        public PostProfile()
        {
            CreateMap<PostAddRequest, PostWithUserData>();
        }
    }
}