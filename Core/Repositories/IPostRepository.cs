using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Entities;

namespace Core.Repositories
{
    public interface IPostRepository
    {
        Task<List<Post>> GetPostsByUserId(int userId, int page = 0);
        Task<bool> AddPost(int authorId, string description, string imageGuid);
        Task<bool> LikeDislikePost(int requesterUserId, string postId);
        Task<bool> AddComment(int commenterId, string postId, string comment);
        Task<List<Comment>> GetComments(string postId, int page);
        Task<List<Post>> GetAllPostsOfUsersByIds(IEnumerable<int> ids, int page);
        Task<List<Post>> GetFullTextSearchedPosts(string phrase);
    }
}