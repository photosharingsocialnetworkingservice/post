using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Contracts;
using Core.Entities;

namespace Core.Services
{
    public interface IPostService
    {
        Task<List<PostWithUserData>> GetPostsById(int userId, int page = 0, int? requestUserId = null);
        Task AddPost(int authorId, string description, string imageGuid);
        Task LikeDislikePost(int requesterUserId, string postId);
        Task AddComment(int commenterId, string postId, string comment);
        Task<List<CommentWithUserData>> GetComments(string postId, int page = 0);
        Task<List<PostWithUserData>> GetBoardPosts(int requestUserId, int page = 0);
        Task<List<PostWithUserData>> GetFullTextSearchedPosts(string phrase, int requestUserId);
    }
}