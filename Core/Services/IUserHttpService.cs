using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Contracts;

namespace Core.Services
{
    public interface IUserHttpService
    {
        Task<EnrichedIdsWithUserDataResponse> GetEnrichedIdsWithUsersData(IEnumerable<int> ids);
        Task<List<int>> GetFriendsIds(int userId);
    }
}