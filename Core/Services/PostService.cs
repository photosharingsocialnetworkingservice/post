using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Core.Contracts;
using Core.Entities;
using Core.Exceptions;
using Core.Repositories;

namespace Core.Services
{
    public class PostService : IPostService
    {
        private readonly IPostRepository _postRepository;
        private readonly IUserHttpService _userHttpService;
        private readonly IMapper _mapper;

        public PostService(
            IPostRepository postRepository,
            IUserHttpService userHttpService,
            IMapper mapper
        )
        {
            _postRepository = postRepository;
            _userHttpService = userHttpService;
            _mapper = mapper;
        }

        public async Task<List<PostWithUserData>> GetPostsById(int userId, int page = 0, int? requestUserId = null)
        {
            var postsWithoutUser = await _postRepository.GetPostsByUserId(userId, page);
            var ids = postsWithoutUser.Select(i => i.AuthorId);
            var postsUsers = (await _userHttpService.GetEnrichedIdsWithUsersData(ids)).EnrichedIds;
            
            var postsWithUsers = postsWithoutUser.Join(postsUsers,
                pwu => pwu.AuthorId,
                pu => pu.Id,
                (pwu, pu) => new PostWithUserData
                {
                    Id = pwu.Id,
                    ImageGuid = pwu.ImageGuid,
                    Description = pwu.Description,
                    CreatedDate = pwu.CreatedDate,
                    Author = pu,
                    IsLiked = requestUserId != null && pwu.LikedBy.Contains((int) requestUserId),
                    LikesCount = pwu.LikedBy.Count
                });

            return postsWithUsers.ToList();
        }

        public async Task LikeDislikePost(int requesterUserId, string postId)
        {
            var isSuccess = await _postRepository.LikeDislikePost(requesterUserId, postId);
            Console.WriteLine($"PostService after request values: {isSuccess}, {requesterUserId}, {postId}");
            if (!isSuccess)
            {
                Console.WriteLine($"PostService after request in if values: {isSuccess}");
                throw new Exception("Could not like or dislike post");
            }
        }
        
        public async Task AddComment(int commenterId, string postId, string comment)
        {
            var isSuccess = await _postRepository.AddComment(commenterId, postId, comment);

            if (!isSuccess)
                throw new AddingNewPostException("Could not add post");
        }
        
        public async Task AddPost(int authorId, string description, string imageGuid)
        {
            var isSuccess = await _postRepository.AddPost(authorId, description, imageGuid);

            if (!isSuccess)
                throw new AddingNewPostException("Could not add post");
        }

        public async Task<List<CommentWithUserData>> GetComments(string postId, int page = 0)
        {
            var comments = await _postRepository.GetComments(postId, page);
            
            if (comments.Count == 0)
                return new List<CommentWithUserData>();
            
            var authorsIds = comments.Select(c => c.AuthorId);
            var enrichedIds = (await _userHttpService.GetEnrichedIdsWithUsersData(authorsIds)).EnrichedIds;
            
            var commentsWithUsers = comments.Join(enrichedIds,
                cwu => cwu.AuthorId,
                ei => ei.Id,
                (cwu, ei) => new CommentWithUserData
                {
                    Id = cwu.Id,
                    Author = ei,
                    Content = cwu.Content,
                    CreatedDate = cwu.CreatedDate
                });

            return commentsWithUsers.ToList();
        }

        public async Task<List<PostWithUserData>> GetBoardPosts(int requestUserId, int page = 0)
        {
            var friendsIds = await _userHttpService.GetFriendsIds(requestUserId);
            var friendsPosts = await _postRepository.GetAllPostsOfUsersByIds(friendsIds, page);
            
            var enrichedFriendsIds = (await _userHttpService.GetEnrichedIdsWithUsersData(friendsIds)).EnrichedIds;
            
            var friendsPostsWithUsersData = friendsPosts.Join(enrichedFriendsIds,
                fp => fp.AuthorId,
                efi => efi.Id,
                (fp, efi) => new PostWithUserData
                {
                    Id = fp.Id,
                    ImageGuid = fp.ImageGuid,
                    Description = fp.Description,
                    CreatedDate = fp.CreatedDate,
                    Author = efi,
                    IsLiked = fp.LikedBy.Contains(requestUserId),
                    LikesCount = fp.LikedBy.Count
                });
            
            return friendsPostsWithUsersData.ToList();
        }

        public async Task<List<PostWithUserData>> GetFullTextSearchedPosts(string phrase, int requestUserId)
        {
            var posts = await _postRepository.GetFullTextSearchedPosts(phrase);
            var authorsIds = posts.Select(s => s.AuthorId);

            var enrichedIdsWithUsersData = (await _userHttpService.GetEnrichedIdsWithUsersData(authorsIds)).EnrichedIds;
         
            var postsWithUserData = posts.Join(enrichedIdsWithUsersData,
                p => p.AuthorId,
                eiwud => eiwud.Id,
                (p, eiwud) => new PostWithUserData
                {
                    Id = p.Id,
                    ImageGuid = p.ImageGuid,
                    Description = p.Description,
                    CreatedDate = p.CreatedDate,
                    Author = eiwud,
                    IsLiked = p.LikedBy.Contains(requestUserId),
                    LikesCount = p.LikedBy.Count
                });

            return postsWithUserData.ToList();
        }
    }
}