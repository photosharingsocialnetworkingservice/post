using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Core.Services
{
    public interface IImageService
    {
        Task SavePostImage(IFormFile file, string path, string fileName);
    }
}