using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Core.Services
{
    public class ImageService : IImageService
    {
        public async Task SavePostImage(IFormFile file, string savePath, string fileName)
        {
            await using var stream = new FileStream(Path.Combine(savePath, fileName), FileMode.Create);
            await file.CopyToAsync(stream);
        }
    }
}