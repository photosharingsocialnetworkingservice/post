using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Core.Contracts;
using Newtonsoft.Json;

namespace Core.Services
{
    public class UserHttpService : IUserHttpService
    {
        private readonly HttpClient _client;

        public UserHttpService(HttpClient client)
        {
            var userServiceBaseUrl = Environment.GetEnvironmentVariable("USER_SERVICE_URL");
            
            if (userServiceBaseUrl == null)
                throw new NoNullAllowedException("User service url env variable does not exist");
            
            client.BaseAddress = new Uri(userServiceBaseUrl);
            _client = client;
        }

        public async Task<EnrichedIdsWithUserDataResponse> GetEnrichedIdsWithUsersData(IEnumerable<int> ids)
        {
            var idsJson = new StringContent(JsonConvert.SerializeObject(ids), Encoding.UTF8, "application/json");
            var response = await _client.PostAsync("enrich/enrichUserIdsByUserData", idsJson);

            EnrichedIdsWithUserDataResponse deserializedResponse;
            
            if (response.IsSuccessStatusCode)
            {
                deserializedResponse = JsonConvert.DeserializeObject<EnrichedIdsWithUserDataResponse>(await response.Content.ReadAsStringAsync());
            }
            else
            {
                deserializedResponse = null;
            }

            return deserializedResponse;
        }

        public async Task<List<int>> GetFriendsIds(int userId)
        {
            var response = await _client.GetAsync($"friends/user?id={userId}");
            
            List<int> deserializedResponse;
            
            if (response.IsSuccessStatusCode)
            {
                deserializedResponse = JsonConvert.DeserializeObject<List<int>>(await response.Content.ReadAsStringAsync());
            }
            else
            {
                deserializedResponse = new List<int>();
            }
            
            return deserializedResponse;
        }
    }
}