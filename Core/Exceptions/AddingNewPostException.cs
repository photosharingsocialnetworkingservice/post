using System;

namespace Core.Exceptions
{
    public class AddingNewPostException : Exception
    {
        public AddingNewPostException()
        {
        }

        public AddingNewPostException(string message) : base(message)
        {
        }

        public AddingNewPostException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}