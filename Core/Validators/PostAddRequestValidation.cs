using System;
using Core.Contracts;
using FluentValidation;

namespace Core.Validators
{
    public class PostAddRequestValidation : AbstractValidator<PostAddRequest>
    {
        public PostAddRequestValidation()
        {
            RuleFor(x => x.Image)
                .Cascade(CascadeMode.Stop)
                .NotNull()
                .Must(x => x.Length < 1024 * 1024)
                .WithMessage("Image max size is 1MB")
                .Must(x =>
                    string.Equals(x.ContentType, "image/jpg", StringComparison.InvariantCultureIgnoreCase) ||
                    string.Equals(x.ContentType, "image/jpeg", StringComparison.InvariantCultureIgnoreCase) ||
                    string.Equals(x.ContentType, "image/png", StringComparison.InvariantCultureIgnoreCase)
                )
                .WithMessage("Image format is not valid. Valid formats: .jpg, .jpeg, .png");

            RuleFor(x => x.Description)
                .NotNull()
                .MaximumLength(250);
        }
    }
}