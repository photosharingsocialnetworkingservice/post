using System;
using Core.Contracts;
using FluentValidation;

namespace Core.Validators
{
    public class CommentAddRequestValidation : AbstractValidator<AddCommentRequest>
    {
        public CommentAddRequestValidation()
        {
            RuleFor(x => x.Comment)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);
        }
    }
}