using System;

namespace Core.Contracts
{
    public class CommentWithUserData
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public UserData Author { get; set; }
    }
}