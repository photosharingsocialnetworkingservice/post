using Microsoft.AspNetCore.Http;

namespace Core.Contracts
{
    public class PostAddRequest
    {
        public string Description { get; set; }
        public IFormFile Image { get; set; }
    }
}