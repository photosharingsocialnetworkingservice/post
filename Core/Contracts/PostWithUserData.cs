using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Core.Contracts
{
    public class PostWithUserData
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public string ImageGuid { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public UserData Author { get; set; }
        public bool IsLiked { get; set; }
        public int LikesCount { get; set; }
    }
}