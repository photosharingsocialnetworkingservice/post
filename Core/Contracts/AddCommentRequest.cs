namespace Core.Contracts
{
    public class AddCommentRequest
    {
        public string Comment { get; set; }
    }
}