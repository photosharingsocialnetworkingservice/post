using System.Collections.Generic;

namespace Core.Contracts
{
    public class EnrichedIdsWithUserDataResponse
    {
        public List<UserData> EnrichedIds { get; set; }
    }
}