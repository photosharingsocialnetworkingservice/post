﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using Core.Contracts;
using Core.Entities;
using Core.Exceptions;
using Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nest;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PostsController : ControllerBase
    {
        private readonly IWebHostEnvironment _env;
        private readonly IPostService _postService;
        private readonly IImageService _imageService;

        public PostsController(
            IWebHostEnvironment env,
            IPostService postService,
            IImageService imageService
        )
        {
            _env = env;
            _postService = postService;
            _imageService = imageService;
        }

        [Authorize]
        [HttpPost("add")]
        public async Task<IActionResult> Add([FromForm]PostAddRequest postAddRequest)
        {
            var requestUserId = int.Parse(User.FindFirstValue("id"));
            
            var wwwrootPath = $"{_env.ContentRootPath}/wwwroot/Images";
            var fileName = Guid.NewGuid() + ".jpg";
            var image = postAddRequest.Image;
            
            await _imageService.SavePostImage(image, wwwrootPath, fileName);
            
            try
            {
                await _postService.AddPost(requestUserId, postAddRequest.Description, fileName);
            }
            catch (AddingNewPostException e)
            {
                return BadRequest(e.Message);
            }
            
            return Ok();
        }
        
        [Authorize]
        [HttpPost("{id}/like")]
        public async Task<IActionResult> LikeDislikePost(string id)
        {
            var requestUserId = int.Parse(User.FindFirstValue("id"));

            try
            {
                await _postService.LikeDislikePost(requestUserId, id);
                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        [Authorize]
        [HttpPost("{id}/comments")]
        public async Task<IActionResult> AddComment(string id, AddCommentRequest addCommentRequest)
        {
            var commenterUserId = int.Parse(User.FindFirstValue("id"));

            try
            {
                await _postService.AddComment(commenterUserId, id, addCommentRequest.Comment);
                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        
        [HttpGet("{id}/comments")]
        public async Task<ActionResult<List<CommentWithUserData>>> GetComments(string id, int page = 0)
        {
            try
            {
                return Ok(await _postService.GetComments(id, page));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("user/{id:int}")]
        public async Task<ActionResult<List<PostWithUserData>>> GetUserPosts(int id, int page = 0)
        {
            var requestUserId = User.FindFirst("id")?.Value;
            int? requestUserIdParsed = null;
                
            if (requestUserId != null)
            {
                requestUserIdParsed = int.Parse(requestUserId);
            }

            return Ok(await _postService.GetPostsById(id, page, requestUserIdParsed));
        }
        
        [HttpGet("board")]
        public async Task<ActionResult<List<PostWithUserData>>> GetBoard(int page = 0)
        {
            var requestUserId = int.Parse(User.FindFirst("id")?.Value!);

            return Ok(await _postService.GetBoardPosts(requestUserId, page));
        }
        
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<List<PostWithUserData>>> FullTextSearch(string query)
        {
            var requestUserId = int.Parse(User.FindFirst("id")?.Value!);
            
            return Ok(await _postService.GetFullTextSearchedPosts(query, requestUserId));
        }
    }
}